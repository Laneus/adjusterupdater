from telneter import Account, Executor
from updater.adjuster_assist import get_hosts, add_port, add_portvlan, delete_port, delete_portvlan

from updater.adjuster_assist import get_ports as get_ports_db
from updater.adjuster_assist import get_portvlans as get_portvlans_db
from updater.executor_assist import get_ports as get_ports_real
from updater.executor_assist import get_portvlans as get_portvlans_real
from updater.executor_assist import get_vlans
from updater.adjuster_assist import check_or_add_vlan

from updater.credentials import acc as ACCOUNT
from threading import Thread
import socket
import sys
import logging
import time



def compare(hostname, db, real):
    result = {'add': [], 'del': []}
    same = set(real) & set(db)
    differencies_in_db = [x for x in db if x not in same]
    differencies_on_device = [x for x in real if x not in same]

    if differencies_in_db:
        logging.debug('[%s] Here are records that must be removed from db' % hostname)
        for r in differencies_in_db:
            logging.debug('[%s]    %r' % (hostname, r))
            result['del'].append(r)
    else:
        logging.debug('[%s] Here are nothing to delete from db' % hostname)

    if differencies_on_device:
        logging.debug('[%s] Here are records that must be added into db' % hostname)
        for r in differencies_on_device:
            logging.debug('[%s]    %r' % (hostname, r))
            result['add'].append(r)
    else:
        logging.debug('[%s] Here are nothing to add into db' % hostname)
    return result


def process_differencies(hostname, differencies, f_add, f_del):
    for obj in differencies['del']:
        logging.debug('[%s] Delete %s ...' % (hostname, obj))
        result = f_del(hostname, obj)
        logging.debug('[%s] %s' % (hostname, result.get('message')))
    for obj in differencies['add']:
        logging.debug('[%s] Adding %s ...' % (hostname, obj))
        result = f_add(hostname, obj)
        logging.debug('[%s] %s' % (hostname, result.get('message')))


def get_records(hostname, obj, real, e, db):
    logging.debug('[%s] Get %s from device...' % (hostname, obj))
    objs_real = real(e)
    logging.debug('[%s] Got it' % hostname)
    logging.debug('[%s] Get %s from Adjuster...' % (hostname, obj))
    objs_db = db(hostname)
    logging.debug('[%s] Got it' % hostname)
    return objs_real, objs_db


def check_vlans(e):
    logging.debug('[%s] Checking vlans...' % e.hostname)
    vlans = get_vlans(e)
    for tag in vlans:
        r = check_or_add_vlan(tag=tag, name=vlans[tag]['name'])
        logging.debug('[%s]    %s: %s' % (e.hostname, str(tag), r))


def check_ports(e):
    ports_real, ports_db = get_records(hostname=e.hostname, obj='Ports',
                                       real=get_ports_real, e=e,
                                       db=get_ports_db)
    differencies = compare(e.hostname, ports_db, ports_real)
    process_differencies(e.hostname, differencies, add_port, delete_port)


def check_portvlans(e):
    portvlans_real, portvlans_db = get_records(hostname=e.hostname,
                                               obj='PortVlans',
                                               real=get_portvlans_real, e=e,
                                               db=get_portvlans_db)
    differencies = compare(e.hostname, portvlans_db, portvlans_real)
    if differencies['add'] or differencies['del']:
        check_vlans(e)
        process_differencies(e.hostname, differencies, add_portvlan, delete_portvlan)


def check_host(host):
    hostname = host.get('name')
    logging.info('[%s] Started' % hostname)
    try:
        logging.debug('[%s] Connecting via telnet...' % hostname)
        e = Executor(hostname, ACCOUNT)
        time.sleep(2)
        logging.debug('[%s] Connected' % hostname)
    except Exception as exception:
        logging.info('[%s] %s' % (hostname, str(exception)))
        return
    logging.info('[%s] Successfully connected' % hostname)
    
    try:
        check_ports(e)
    except Exception as exception:
        logging.info('[%s] Exception during checking Ports: %s' % (hostname,
                                                          str(exception)))
    logging.info('[%s] Ports checked' % hostname)

    try:
        check_portvlans(e)
    except Exception as exception:
        logging.info('[%s] Exception during checking PortVlans: %s' % (hostname,
                                                              str(exception)))
    logging.info('[%s] PortVlans checked' % hostname)
    e.close()
    logging.info('[%s] Successfully finished' % hostname)
