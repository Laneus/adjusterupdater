from collections import namedtuple
from lxml import etree
import re
import requests

Port = namedtuple('Port', ['hostname', 'name', 'description'])


def get_ports(e, ignore_subs=True):
    "returns list of all Port objects from JunOS switch"
    ports_raw = e.cmd('show interfaces descriptions | no-more')
    ports = [Port(e.hostname, x[0], x[-1])
             for x in
             [y.split() for y in ports_raw.splitlines()[2:-3]]]
    if ignore_subs:
        ports = [x for x in ports if '.' not in x.name]
    return ports


def _cmd_xml(e, command):
    raw = e.run_and_expect('{} | display xml'.format(command), ".*> $", 60)
    if raw:
        raw = raw[-1]
    matched = re.search(r'<rpc-reply(.|[\r]|[\r\n])*rpc-reply>', raw)
    if matched:
        rpc_reply = matched.group()
        return etree.fromstring(rpc_reply)


def get_vlans(e):
    if '-r' in e.hostname:
        return _get_vlans_router(e)
    else:
        return _get_vlans_switch(e)


def _get_vlans_router(e):
    vlans = {}
    ports = get_ports(e, ignore_subs=False)
    for port in ports:
        if '.' in port.name:
            tag = int(port.name.split('.')[-1])
            if tag != 0:
                name = port.name.split('.')[0]
                description = port.description
                if not vlans.get(tag):
                    vlans[tag] = {'untagged': [], 'tagged': [], 'name': description}
                vlans[tag]['tagged'].append(name)
    return vlans


def _get_vlans_switch(e):
    "returns dict {VLAN_TAG: {'name':VLAN_NAME,'tagged':PORTS, 'untagged':PORTS}}"
    result = {}

    vlans = _cmd_xml(e, 'show vlans')
    try:
        vlan_instances = vlans.xpath("//*[contains(name(), 'vlan-tag')]/..")
    except AttributeError:
        return _get_vlans_router(e)

    for vlan in vlan_instances:
        tag_list = vlan.xpath("*[contains(name(), 'vlan-tag')]/text()")
        tag = int(tag_list[0]) if tag_list else None
        name_list = vlan.xpath("*[contains(name(), 'vlan-name')]/text()")
        name = name_list[0] if name_list else None
        if not tag:
            continue
        if not result.get(tag):
            result[tag] = {'tagged': [], 'untagged': [], 'name': name}

        ports = vlan.xpath("*//*[contains(name(), 'member-interface')]/text()")
        result[tag]['tagged'].extend([x.replace('*', '').replace('.0','') for x in ports if '*' in x])
        result[tag]['untagged'].extend([x.replace('.0', '') for x in ports if '*' not in x])
    return result
