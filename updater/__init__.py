from updater.functions import get_hosts, check_host
from threading import Thread
import sys
import logging
logging.basicConfig(format=u'[%(asctime)s] %(message)s',
                    level=logging.INFO,
                    # level=logging.DEBUG,
                    stream=sys.stdout)


def update():
    hosts = get_hosts()
    hosts = [x for x in hosts if x.get('os') in ['JUNOS', 'EXOS']]
    threads = []
    for host in hosts:
        threads.append(Thread(target=check_host, args=(host,)))
        threads[-1].start()
    for t in threads:
        t.join()

