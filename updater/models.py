from collections import namedtuple

Port = namedtuple('Port', ['hostname', 'name', 'description'])
PortVlan = namedtuple('PortVlan', ['port_name', 'vlan_tag', 'tagged'])
