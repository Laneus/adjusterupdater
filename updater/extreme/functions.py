from collections import namedtuple
import itertools
import requests

Port = namedtuple('Port', ['hostname', 'name', 'description'])


def get_ports(e):
    "returns list of all Port objects from EXOS switch"
    ports_raw = e.cmd('show configuration | include "display"')
    ports = [Port(e.hostname, x[2], x[-1])
             for x in
             [y.split() for y in ports_raw.splitlines()[2:-1]]]
    return ports


def _get_tags(raw):
    "returns dict where keys are VLAN_TAG and values are VLAN_NAME"
    tags = {}
    vlans_tags = [x.split() for x in raw.splitlines() if 'tag ' in x]
    for v in vlans_tags:
        tags[v[2]] = v[-1]
    return tags


def _get_vlans(raw):
    "returns list with (VLAN_NAME,TAGGED_OR_NOT, PORTS)"
    vlans_filtered = [x.split() for x in raw.splitlines() if 'add ' in x]
    vlans_splitted = [(x[2], x[-1], x[5:-1]) for x in vlans_filtered]
    final = []
    for v in vlans_splitted:
        ports = v[-1]
        if len(ports) == 1:
            final_ports = list(itertools.chain.from_iterable([x.split(',') for x in ports]))
        else:
            final_ports = ports
        final.append((v[0], v[1], final_ports))
    return final


def _parse_ports(ports):
    "converts ['32-35,', '49'] to [32,33,34,35,49]"
    ports_ranges = [list(map(int, p.replace(',', '').split('-'))) for p in ports]
    ports_lists = [list(range(x[0], x[-1]+1)) for x in ports_ranges]
    ports_int = list(itertools.chain.from_iterable(ports_lists))
    ports_str = list(map(str, ports_int))
    return ports_str


def get_vlans(e):
    "returns dict {VLAN_TAG: {'name':VLAN_NAME,'tagged':PORTS, 'untagged':PORTS}}"
    result = {}
    vlans_raw = e.cmd('show configuration | include "configure vlan"')
    tags = _get_tags(vlans_raw)
    vlans = _get_vlans(vlans_raw)
    for vlan in vlans:
        name = vlan[0]
        tag = int(tags[name])
        tagged = vlan[1]
        ports = _parse_ports(vlan[-1])
        if not result.get(tag):
            result[tag] = {'tagged': [], 'untagged': [], 'name': name}
        result[tag][tagged].extend(ports)
    return result
