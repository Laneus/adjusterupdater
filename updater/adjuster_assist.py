from updater.models import Port, PortVlan
import requests
import logging
import os

API = os.environ.get('ADJUSTER_URL')
if API is None:
    raise ValueError('ADJUSTER_URL is not defined')

def get_hosts():
    url = API + '/hosts?limit=999'
    return requests.get(url).json().get('data')


def get_ports(hostname):
    ports_result = []
    url = API + '/hosts/' + hostname + '/ports?limit=999&fields=name,description'
    ports_raw = requests.get(url).json().get('data')
    for port in ports_raw:
        ports_result.append(Port(hostname=hostname,
                                name=port['name'],
                                description=port['description']))
    return ports_result


def get_portvlans(hostname):
    portvlans = []
    url = API + '/hosts/' + hostname + '/ports?limit=999&fields=name,vlans.tagged,vlans.vlan.tag,vlans.vlan.name'
    portvlans_raw = requests.get(url).json().get('data')
    for port in portvlans_raw:
        for record in port.get('vlans', []):
            portvlans.append(PortVlan(port_name=port.get('name'),
                                      vlan_tag=record['vlan']['tag'],
                                      tagged=record['tagged']))
    return portvlans


def delete_port(hostname, port):
    portname = port.name.replace('/', '_')
    url = API + '/hosts/' + hostname + '/ports/' + portname
    result = requests.delete(url).json()
    return result


def add_port(hostname, port):
    portname = port.name
    description = port.description
    url = API + '/hosts/' + hostname + '/ports'
    result = requests.post(url, json=dict(name=portname, description=description)).json()
    return result


def delete_portvlan(hostname, portvlan):
    portname = portvlan.port_name.replace('/', '_')
    vlan_tag = portvlan.vlan_tag
    url = API + '/hosts/' + hostname + '/ports/' + portname + '/vlans/' + str(vlan_tag)
    result = requests.delete(url).json()
    return result


def add_portvlan(hostname, portvlan):
    portname = portvlan.port_name.replace('/', '_')
    vlan_tag = portvlan.vlan_tag
    tagged = portvlan.tagged
    url = API + '/hosts/' + hostname + '/ports/' + portname + '/vlans'
    result = requests.post(url, json=dict(tag=vlan_tag, tagged=tagged)).json()
    return result


def check_or_add_vlan(tag, name):
    request = requests.get(API+'/vlans/' + str(tag))
    status = request.status_code
    if status == 200:
        return 'Ok'
    elif status == 404:
        request = requests.post(API+'/vlans', json=dict(name=name, tag=tag))
        return 'Added'
