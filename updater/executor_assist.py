import updater.extreme.functions as exos
import updater.juniper.functions as junos
from updater.models import Port, PortVlan

def get_ports(e):
    if e.os == 'EXOS':
        return exos.get_ports(e)
    if e.os == 'JUNOS':
        return junos.get_ports(e)
    else:
        return None

def get_portvlans(e):
    _vlans = get_vlans(e)
    portvlans = []
    for tag in _vlans:
        vlan = _vlans[tag]
        vlan_name = vlan.get("name")
        if vlan_name != '0':
            for port in vlan.get('tagged', []):
                if port != 'None':
                    portvlans.append(PortVlan(port_name=port, vlan_tag=tag, tagged=True))
            for port in vlan.get('untagged', []):
                if port != 'None':
                    portvlans.append(PortVlan(port_name=port, vlan_tag=tag, tagged=False))
    return portvlans


def get_vlans(e):
    if e.os == 'EXOS':
        return exos.get_vlans(e)
    if e.os == 'JUNOS':
        return junos.get_vlans(e)
    else:
        return None
