from flask import Flask, jsonify
from updater import update
from datetime import datetime

print('Ready.')

app = Flask(__name__)

@app.route('/update')
def _update():
    t0 = datetime.now()
    update()
    t = datetime.now() - t0
    return jsonify(dict(message='Updated',
                        performed=str(t.seconds)+' seconds'))
