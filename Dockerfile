FROM ubuntu:latest
RUN apt-get update -y
RUN apt-get install -y python-pip python-dev build-essential
COPY . /app
WORKDIR /app
RUN pip install --upgrade pip
RUN pip install gunicorn
RUN pip install -r requirements.txt
ENTRYPOINT ["gunicorn", "-w", "1","-t","120","-b","0.0.0.0:8000", "app:app"]
